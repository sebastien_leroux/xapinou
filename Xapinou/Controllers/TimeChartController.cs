﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Xapinou.Models;
using Xapinou.Attributes;
using Xapinou.Common;

namespace Xapinou.Controllers
{
    public class TimeChartController : AuthenticatedApiController
    {
        public TimeChartController()
            : base()
        {
            ;
        }

        // GET api/timechart
        //
        public TimeChart Get()
        {

            return MakeAggregations();
        }

        private TimeChart MakeAggregations()
        {
            // Per Hour Chart.

            Interval daily = IntervalUtil.Daily();

            var today = from x in XapinouContext.Activities
                        where x.UserId == UserId && x.TimeStamp >= daily.StartDate && x.TimeStamp <= daily.EndDate
                        select new
                        {
                            TimeStamp = x.TimeStamp,
                        };

            var perhourtoday = from t in today
                               group t by t.TimeStamp.Hour into g
                               select new PerHour
                               {
                                   Time = g.Key,
                                   Count = (int)g.Count()
                               };
            
            Interval daybefore = IntervalUtil.Yesterday();

            var yesterday = from x in XapinouContext.Activities
                            where x.UserId == UserId && x.TimeStamp >= daybefore.StartDate && x.TimeStamp <= daybefore.EndDate
                            select new
                            {
                                TimeStamp = x.TimeStamp,
                            };
            var perhouryesterday = from y in yesterday
                                   group y by y.TimeStamp.Hour into g
                                   select new PerHour
                                   {
                                       Time = g.Key,
                                       Count = (int)g.Count()
                                   };

            // Per Week Chart.

            Interval weekly = IntervalUtil.Weekly();

            var week = from x in XapinouContext.Activities
                       where x.UserId == UserId && x.TimeStamp >= weekly.StartDate && x.TimeStamp <= weekly.EndDate
                       select new
                       {
                           Year = x.TimeStamp.Year,
                           Month = x.TimeStamp.Month,
                           Day = x.TimeStamp.Day,
                       };


            var perweek = from w in week
                          group w by new
                          {
                              w.Year,
                              w.Month,
                              w.Day
                          } into g
                          select new PerWeek
                          {
                              Day = g.Key.Day,
                              Month = g.Key.Month,
                              Year = g.Key.Year,
                              Count = (int)g.Count()
                          };

            return new TimeChart
            {
                PerHourToday = perhourtoday.ToList<PerHour>(),
                PerHourYesterday = perhouryesterday.ToList<PerHour>(),
                PerWeek = perweek.ToList<PerWeek>(),
            };
        }
    }
}
