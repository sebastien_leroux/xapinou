﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Threading;
using System.Threading.Tasks;
using Xapinou.Common;
using Xapinou.Attributes;

namespace Xapinou.Controllers
{
    public class AuthenticatedApiController : XapinouApiBaseController
    {
        private int _userId;

        /// <summary>
        /// modification GIT
        /// </summary>
        public int UserId
        {
            get
            {
                return _userId;
            }
        }

        public AuthenticatedApiController()
            : base()
        {
            ;
        }

        public override Task<HttpResponseMessage> ExecuteAsync(HttpControllerContext controllerContext, CancellationToken cancellationToken)
        {
            string authorization = ParseContext(controllerContext);

            if (String.IsNullOrEmpty(authorization))
            {
                return Unauthorized();

            }
            else
            {
                if (!IsAuthorized(authorization))
                {
                    return Unauthorized();
                }
                else
                {
                    _userId = int.Parse(GetUserId(authorization));
                }
            }

            return base.ExecuteAsync(controllerContext, cancellationToken);
        }

        private Task<HttpResponseMessage> Unauthorized()
        {
            var rsp = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            var tcs = new TaskCompletionSource<HttpResponseMessage>();

            tcs.SetResult(rsp);

            return tcs.Task;
        }

        private string GetUserId(string authorization)
        {
            string[] authTab = SplitInitialAndHash(authorization);

            string initial = authTab[0];
            string userId = initial.Split(';')[0].ToString();

            return userId;
        }

        private bool IsAuthorized(string authorization)
        {
            string salt = Salt.Get();

            string[] authTab = SplitInitialAndHash(authorization);

            string initial = authTab[0];
            string hash = authTab[1];

            string initialSalted = initial + ";" + salt;
            string initialSaltedHash = initialSalted.ToMd5();

            return (initialSaltedHash == hash);

        }

        private string ParseContext(HttpControllerContext controllerContext)
        {
            KeyValuePair<string, IEnumerable<string>> x = controllerContext.Request.Headers.SingleOrDefault(a => a.Key == "Authorization");

            return x.Value.SingleOrDefault().ToString();
        }

        private string[] SplitInitialAndHash(string auth)
        {
            return auth.Split('|');
        }
    }
}
