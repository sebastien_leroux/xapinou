﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Xapinou.Attributes;
using Xapinou.Models;

namespace Xapinou.Controllers
{

    public class SmokeController : AuthenticatedApiController
    {
        public SmokeController()
            : base()
        {
            ;
        }

        // POST api/smoke
        //

        public void Post([FromBody] Activity activity)
        {
            if (ModelState.IsValid)
            {
                // Get GeoLocationId if already exists.
                var geoLocationId = (from l in XapinouContext.GeoLocations
                                     where l.Latitude == activity.CurrentLatitude
                                        && l.Longitude == activity.CurrentLongitude
                                        && l.UserId == UserId
                                     select l.Id).FirstOrDefault();

                // Insert new GeoLocationId if not exists with default PlacedId = 0.
                if (geoLocationId == 0)
                {
                    GeoLocation geoLocation = new GeoLocation
                    {
                        Latitude = activity.CurrentLatitude,
                        Longitude = activity.CurrentLongitude,
                        UserId = UserId,
                        PlaceId = 0,
                    };

                    XapinouContext.GeoLocations.Add(geoLocation);
                    XapinouContext.SaveChanges();

                    geoLocationId = geoLocation.Id;
                }


                // Insert activity with geoLocationId.
                XapinouContext.Activities.Add(new Activity
                {
                    ActivityTypeId = 1,
                    GeoLocationId = geoLocationId,
                    UserId = UserId,
                    TimeStamp = activity.TimeStamp,
                });

                XapinouContext.SaveChanges();
            }
        }
    }
}
