﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xapinou.ViewModels;

namespace Xapinou.Controllers
{
    public class IndexController : Controller
    {
        public IndexController()
            : base()
        {
            ;
        }

        [HttpGet]
        public ViewResult Home()
        {
            return View("~/Views/Pages/Home.cshtml");
        }

        [HttpGet]
        public ViewResult Analytics()
        {
            return View("~/Views/Pages/Analytics.cshtml");
        }

        [HttpGet]
        public ViewResult Maps()
        {
            return View("~/Views/Pages/Maps.cshtml");
        }

        [HttpGet]
        public PartialViewResult Places(double latitude, double longitude, int placeId, int count)
        {
            SelectPlaceViewModel vw = new SelectPlaceViewModel(latitude, longitude, placeId, count);

            return PartialView("~/Views/Pages/_PlacesOptions.cshtml", vw);
        }
    }
}
