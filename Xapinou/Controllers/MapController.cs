﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Xapinou.Attributes;
using Xapinou.Models;
using Xapinou.Common;

namespace Xapinou.Controllers
{
    public class MapController : AuthenticatedApiController
    {
        // GET api/map
        //
        public Map Get()
        {
            return MakeAggregations();
        }

        private Map MakeAggregations()
        {

            // Per Location.

            Interval daily = IntervalUtil.Daily();

            var activities = from x in XapinouContext.Activities
                             join l in XapinouContext.GeoLocations on x.GeoLocationId equals l.Id
                             where x.UserId == UserId && x.TimeStamp >= daily.StartDate && x.TimeStamp <= daily.EndDate
                             select new
                             {
                                 TimeStamp = x.TimeStamp,
                                 Latitude = l.Latitude,
                                 Longitude = l.Longitude,
                                 PlaceId = l.PlaceId,
                             };


            var perloc = from a in activities
                         group a by new
                         {
                             a.Latitude,
                             a.Longitude,
                             a.PlaceId
                         } into g
                         select new PerLocation
                         {
                             Latitude = g.Key.Latitude,
                             Longitude = g.Key.Longitude,
                             PlaceId = g.Key.PlaceId,
                             Count = (int)g.Count()
                         };

            // Per Place Chart.

            var place = from x in XapinouContext.Activities
                        join l in XapinouContext.GeoLocations on x.GeoLocationId equals l.Id
                        where x.UserId == UserId && x.TimeStamp >= daily.StartDate && x.TimeStamp <= daily.EndDate
                        select new
                        {
                            PlaceId = l.PlaceId,
                        };

            var perplace = from p in place
                           group p by p.PlaceId into g
                           select new PerPlace
                           {
                               PlaceId = g.Key,
                               Count = (int)g.Count()
                           };

            return new Map
            {
                PerLocation = perloc.ToList<PerLocation>(),
                PerPlace = perplace.ToList<PerPlace>(),
            };
        }
    }
}
