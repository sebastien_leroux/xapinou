﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Xapinou.Models;
using Xapinou.Common;

namespace Xapinou.Controllers
{
    public class UserController : XapinouApiBaseController
    {
        public UserController()
            : base()
        {
            ;
        }

        // Post: api/user
        //
        [AllowAnonymous]
        public string Post([FromBody] User user)
        {
            if (!String.IsNullOrEmpty(user.Name) && !String.IsNullOrEmpty(user.UserPassword))
            {
                var authUser = TryAuthenticate(user.Name, user.UserPassword);

                if (authUser != null)
                {
                    return EncodeAuthToken(authUser);
                }
                
                var newUser = CreateUser(user);

                if (newUser != null)
                {
                    return EncodeAuthToken(newUser);
                }

                return "400";
            }

            return "400";
        }

        private string EncodeAuthToken(User user)
        {
            return user.Encode(Salt.Get());
        }

        private User TryAuthenticate(string username, string password)
        {
            var hashPassword = password.ToMd5();

            return XapinouContext.Users.FirstOrDefault(u => u.Name.ToLower() == username.ToLower() && u.Password == hashPassword);
        }

        private User CreateUser(User user)
        {
            user.Token = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            user.Password = user.UserPassword.ToMd5();

            XapinouContext.Users.Add(user);
            XapinouContext.SaveChanges();

            return TryAuthenticate(user.Name, user.UserPassword);
        }
    }
}
