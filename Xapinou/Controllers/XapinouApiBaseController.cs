﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Xapinou.Controllers {
    public class XapinouApiBaseController : ApiController {
        private XapinouContext _xapinouContext;

        public XapinouApiBaseController()
            : base() {
            _xapinouContext = new XapinouContext();
        }

        public XapinouContext XapinouContext {
            get {
                return _xapinouContext;
            }
        }
    }
}
