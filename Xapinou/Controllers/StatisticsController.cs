﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Xapinou.Models;
using Xapinou.Attributes;
using Xapinou.Common;

namespace Xapinou.Controllers
{
    public class StatisticsController : AuthenticatedApiController
    {
        // GET api/statistics
        //
        public Statistics Get()
        {
            return MakeAggregations();
        }

        private Statistics MakeAggregations()
        {

            Interval daily = IntervalUtil.Daily();
            Interval weekly = IntervalUtil.Weekly();
            Interval monthly = IntervalUtil.Monthly();
            Interval yearly = IntervalUtil.Yearly();

            int dailycount = (from x in XapinouContext.Activities
                              where x.UserId == UserId && x.TimeStamp >= daily.StartDate && x.TimeStamp <= daily.EndDate
                              select x.Id).Count();

            int weeklycount = (from x in XapinouContext.Activities
                               where x.UserId == UserId && x.TimeStamp >= weekly.StartDate && x.TimeStamp <= weekly.EndDate
                               select x.Id).Count();

            int monthlycount = (from x in XapinouContext.Activities
                                where x.UserId == UserId && x.TimeStamp >= monthly.StartDate && x.TimeStamp <= monthly.EndDate
                                select x.Id).Count();

            int yearlycount = (from x in XapinouContext.Activities
                               where x.UserId == UserId && x.TimeStamp >= yearly.StartDate && x.TimeStamp <= yearly.EndDate
                               select x.Id).Count();

            DateTime lastSmoked = (from x in XapinouContext.Activities
                                   where x.UserId == UserId
                                   orderby x.TimeStamp descending
                                   select x.TimeStamp).Take(1).SingleOrDefault();

            return new Statistics
            {
                DailyCount = dailycount,
                WeeklyCount = weeklycount,
                MonthlyCount = monthlycount,
                YearlyCount = yearlycount,
                LastSmoked = lastSmoked
            };
        }

    }
}
