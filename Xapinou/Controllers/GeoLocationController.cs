﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Xapinou.Models;
using Xapinou.Attributes;

namespace Xapinou.Controllers
{
    public class GeoLocationController : AuthenticatedApiController
    {
        public GeoLocationController()
            : base()
        {
            ;
        }

        // POST api/geolocation
        //

        public void Post([FromBody]GeoLocation geoLocation)
        {
            if (ModelState.IsValid)
            {
                // Get GeoLocation if already exists.
                var gl = (from l in XapinouContext.GeoLocations
                          where l.Latitude == geoLocation.Latitude
                             && l.Longitude == geoLocation.Longitude
                             && l.UserId == UserId
                          select l).FirstOrDefault();

                if (gl != null)
                {
                    gl.PlaceId = geoLocation.PlaceId;

                    XapinouContext.SaveChanges();
                }
            }
        }
    }
}
