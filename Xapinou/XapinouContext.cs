﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Xapinou.Models;

namespace Xapinou
{
    public class XapinouContext : DbContext
    {

        public XapinouContext()
            : base("name=XapinouConnection")
        {
            ;
        }

        public DbSet<User> Users
        {
            get;
            set;
        }
        public DbSet<ActivityType> ActivityTypes
        {
            get;
            set;
        }
        public DbSet<Activity> Activities
        {
            get;
            set;
        }
        public DbSet<GeoLocation> GeoLocations
        {
            get;
            set;
        }
    }
}