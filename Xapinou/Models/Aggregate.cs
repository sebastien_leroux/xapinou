﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xapinou.Models
{
    public class PerLocation
    {
        public double Latitude
        {
            get;
            set;
        }

        public double Longitude
        {
            get;
            set;
        }

        public int PlaceId
        {
            get;
            set;
        }

        public int Count
        {
            get;
            set;
        }
    }

    public class PerHour
    {
        public int Time
        {
            get;
            set;
        }

        public int Count
        {
            get;
            set;
        }
    }

    public class PerWeek
    {

        public DayOfWeek DayOfWeek
        {
            get
            {
                return new DateTime(Year, Month, Day).DayOfWeek;
            }
        }

        public int Year
        {
            get;
            set;
        }
        public int Month
        {
            get;
            set;
        }
        public int Day
        {
            get;set;

        }
        public int Count
        {
            get;
            set;
        }
    }

    public class PerPlace
    {
        public int PlaceId
        {
            get;
            set;
        }

        public int Count
        {
            get;
            set;
        }
    }
}