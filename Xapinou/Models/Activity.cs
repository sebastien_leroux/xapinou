﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Xapinou.Common;

namespace Xapinou.Models
{
    [Table("Activity")]
    public class Activity
    {
        private double _latitude;
        private double _longitude;

        [Key, Column("Id")]
        public int Id
        {
            get;
            set;
        }

        [Column("User_Id")]
        public int UserId
        {
            get;
            set;
        }


        [Column("Activity_Type_Id")]
        public Int16 ActivityTypeId
        {
            get;
            set;
        }


        [Column("Timestamp")]
        public DateTime TimeStamp
        {
            get;
            set;
        }

        [Column("Geo_Location_Id")]
        public int GeoLocationId
        {
            get;
            set;
        }

        [NotMapped]
        public double CurrentLatitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                _latitude = Math.Round(value, 2);
            }
        }

        [NotMapped]
        public double CurrentLongitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                _longitude = Math.Round(value, 2);
            }
        }
    }
}