﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xapinou.Models
{
    public class Map
    {
        // Render the daily map.
        //
        public IList<PerLocation> PerLocation
        {
            get;
            set;
        }

        // Render a pie chart.
        //
        public IList<PerPlace> PerPlace
        {
            get;
            set;
        }
    }
}