﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Xapinou.Common;

namespace Xapinou.Models
{
    [Table("Geo_Location")]
    public class GeoLocation
    {
        [Key, Column("Id")]
        public int Id
        {
            get;
            set;
        }

        [Column("Latitude")]
        public double Latitude
        {
            get;
            set;
        }

        [Column("Longitude")]
        public double Longitude
        {
            get;
            set;
        }

        [Column("User_Id")]
        public int UserId
        {
            get;
            set;
        }

        [Column("Place_Id")]
        public Int16 PlaceId
        {
            get;
            set;
        }
    }
}