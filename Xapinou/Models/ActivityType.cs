﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Xapinou.Common;

namespace Xapinou.Models
{
    [Table("Activity_Type")]
    public class ActivityType
    {

        [Key, Column("Id")]
        public Int16 Id
        {
            get;
            set;
        }

        [Column("Name")]
        public string Name
        {
            get;
            set;
        }
    }
}