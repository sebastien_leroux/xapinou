﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xapinou.Models
{
    public class TimeChart
    {
        // Render a bar chart.
        //
        public IList<PerHour> PerHourToday
        {
            get;
            set;
        }

        public IList<PerHour> PerHourYesterday
        {
            get;
            set;
        }

        // Render a radar chart.
        //
        public IList<PerWeek> PerWeek
        {
            get;
            set;
        }
    }
}