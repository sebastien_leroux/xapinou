﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xapinou.Models
{
    public class Statistics
    {
        private DateTime _lastSmoked;

        public int DailyCount
        {
            get;
            set;
        }

        public int WeeklyCount
        {
            get;
            set;
        }

        public int MonthlyCount
        {
            get;
            set;
        }

        public int YearlyCount
        {
            get;
            set;
        }

        public DateTime LastSmoked
        {
            get
            {
                return _lastSmoked;
            }
            set
            {
                _lastSmoked = value;
            }
        }

        public string HumanLastSmoked
        {
            get
            {
                return _lastSmoked.ToString("dd/MM/yyyy") + " à " + _lastSmoked.ToString("HH:mm");
            }
        }
    }
}