﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Xapinou.Common;

namespace Xapinou.Models
{
    [Table("User")]
    public class User
    {

        [Key, Column("Id")]
        public int Id
        {
            get;
            set;
        }

        [Required, Column("Name")]
        public string Name
        {
            get;
            set;
        }

        [Column("Password")]
        public string Password
        {
            get;
            set;
        }

        [Required, NotMapped]
        public string UserPassword
        {
            get;
            set;
        }

        [Column("Token")]
        public string Token
        {
            get;
            set;
        }

        /// <summary>
        /// id;name;token;salt -> hash
        /// 
        /// </summary>
        /// <param name="salt"></param>
        /// <returns>id;name;token|hash</returns>
        public string Encode(string salt)
        {
            string initial = this.Id.ToString() + ";" + this.Name + ";" + this.Token.ToString();
            string salted = initial + ";" + salt;
            string hash = salted.ToMd5();

            return initial + "|" + hash;
        }
    }
}