﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xapinou.ViewModels
{
    public class SelectPlaceViewModel
    {
        private double _latitude;
        private double _longitude;
        private int _placeId;
        private int _count;
        private string _glyphicon;

        public double Latitude
        {
            get
            {
                return _latitude;
            }
        }

        public double Longitude
        {
            get
            {
                return _longitude;
            }
        }

        public int PlaceId
        {
            get
            {
                return _placeId;
            }
        }

        public int Count
        {
            get
            {
                return _count;
            }
        }

        public string GlyphIcon
        {
            get
            {
                return _glyphicon;
            }
        }

        public SelectPlaceViewModel()
        {
            ;
        }

        public SelectPlaceViewModel(double latitude, double longitude, int placeId, int count)
        {
            _latitude = latitude;
            _longitude = longitude;
            _placeId = placeId;
            _count = count;
            _glyphicon = GetGlyphicon();
        }

        private string GetGlyphicon()
        {
            string tmp;

            switch (_placeId)
            {
                case 0:
                    tmp = "glyphicon-question-sign";
                    break;
                case 1:
                    tmp = "glyphicon-home";
                    break;
                case 2:
                    tmp = "glyphicon-briefcase";
                    break;
                case 3:
                    tmp = "glyphicon-education";
                    break;
                case 4:
                    tmp = "glyphicon-user";
                    break;
                case 5:
                    tmp = "glyphicon-glass";
                    break;
                case 6:
                    tmp = "glyphicon-cutlery";
                    break;
                case 7:
                    tmp = "glyphicon-map-marker";
                    break;
                default:
                    tmp = "glyphicon-question-sign";
                    break;
            };

            return tmp;
        }
    }
}