﻿/* Accueil */

function _Timestamp() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var hh = today.getHours();
    var nn = today.getMinutes();
    var ss = today.getSeconds();

    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }

    if (hh < 10) {
        hh = '0' + hh
    }

    if (nn < 10) {
        nn = '0' + nn
    }
    if (ss < 10) {
        ss = '0' + ss
    }
    var today = mm + '/' + dd + '/' + yyyy + ' ' + hh + ':' + nn + ':' + ss;

    $('#timestamp').val(today);
}

function _Count(jsonstats) {
    $('#count-daily').text(jsonstats.DailyCount);
    $('#count-weekly').text(jsonstats.WeeklyCount);
    $('#count-monthly').text(jsonstats.MonthlyCount);
    //$('#count-yearly').text(jsonstats.YearlyCount);

    $('#main-count-daily').text(jsonstats.DailyCount);
    $('#last-smoked').text(jsonstats.HumanLastSmoked);
}

function _CurrentLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            $("#currentlatitude").val(position.coords.latitude);
            $("#currentlongitude").val(position.coords.longitude);
        });
    }
}

/* Load */

function _ReLoad(auth) {
    var _u = auth.split(';'),
        _username = _u[1]
    ;

    $('#authname').text(_username);

    $('.anonymous').hide();
    $('.authenticated').show();

    _CurrentLocation();
    _Timestamp();

    $.ajax({
        type: 'GET',
        url: 'api/statistics',
        dataType: 'text',
        beforeSend: function (jqXHR) {
            jqXHR.setRequestHeader('Authorization', auth);
        },
        success: function (data) {
            jsonstats = JSON.parse(data);

            _Count(jsonstats);
        }
    });
}

$(function () {
    var auth = localStorage.getItem("xapiauth");

    var jsonstats = {};

    if (auth != undefined) {
        _ReLoad(auth);
    } else {
        $('.anonymous').show();
        $('.authenticated').hide();
    }

    // Bind AjaxStart.
    $(document).ajaxStart(function () {
        $('#main-count-daily').hide();
        $('#wait').show();
    }).ajaxComplete(function () {
        $('#main-count-daily').show();
        $('#wait').hide();
    });

    // SmokeMe.
    $smokeme = $('#smokeme');

    $smokeme.on('submit', function (e) {
        e.preventDefault;

        $.ajax({
            type: $smokeme.attr('method'),
            url: $smokeme.attr('action'),
            beforeSend: function (jqXHR) {
                jqXHR.setRequestHeader('Authorization', auth);
            },
            data: $smokeme.serialize(),
            success: function (data) {
                _ReLoad(auth);
            },
            Error: function (jqXHR, textStatus, errorThrown) {
                $('#smoke-error').show();
            }
        });

        return false;
    });

    // SignupMe.
    $signupme = $('#signupme');

    $signupme.on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            type: $signupme.attr('method'),
            url: $signupme.attr('action'),
            data: $signupme.serialize(),
            success: function (data) {
                if (data != "400") {
                    localStorage.setItem("xapiauth", data);

                    window.location.reload();
                } else {
                    alert("Bad request");
                }
            }
        });

        return false;
    });


});