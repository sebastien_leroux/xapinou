﻿
/* Charts */

function _WeeklyChart(datas) {
    var ctx = document.getElementById("weekly-chart").getContext("2d");

    var weekly = new Array();

    for (i = 0; i <= 6; i++) {
        weekly[i] = 0;
    }

    $.each(datas, function (k, item) {
        weekly[item.DayOfWeek] = item.Count || 0;
    });

    var data = {
        labels: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
        datasets: [
            {
                label: "This Week",
                fillColor: "rgba(51, 122, 183,.5)",
                strokeColor: "rgba(51, 122, 183,1)",
                pointColor: "rgba((51, 122, 183,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(51, 122, 183,1)",
                data: weekly
            }
        ]
    };

    var options = {
        responsive: false,

        //Boolean - Whether to show lines for each scale point
        scaleShowLine: true,

        //Boolean - Whether we show the angle lines out of the radar
        angleShowLineOut: true,

        //Boolean - Whether to show labels on the scale
        scaleShowLabels: false,

        // Boolean - Whether the scale should begin at zero
        scaleBeginAtZero: true,

        //String - Colour of the angle line
        angleLineColor: "rgba(0, 0, 0, .1)",

        //Number - Pixel width of the angle line
        angleLineWidth: 1,

        //String - Point label font declaration
        pointLabelFontFamily: "'Arial'",

        //String - Point label font weight
        pointLabelFontStyle: "normal",

        //Number - Point label font size in pixels
        pointLabelFontSize: 10,

        //String - Point label font colour
        pointLabelFontColor: "#666",

        //Boolean - Whether to show a dot for each point
        pointDot: true,

        //Number - Radius of each point dot in pixels
        pointDotRadius: 3,

        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth: 1,

        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius: 20,

        //Boolean - Whether to show a stroke for datasets
        datasetStroke: true,

        //Number - Pixel width of dataset stroke
        datasetStrokeWidth: 2,

        //Boolean - Whether to fill the dataset with a colour
        datasetFill: true,

        //String - A legend template
        //legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

    };

    var c = new Chart(ctx).Radar(data, options);
}

function _DailyChart(todaydatas, yesterdaydatas) {
    var ctx = document.getElementById("daily-chart").getContext("2d");

    var daily = new Array();
    var yesterday = new Array();

    for (i = 0; i <= 23; i++) {
        var yfound = 0, tfound = 0;

        $.each(todaydatas, function (k, item) {
            if (i == item.Time) {
                tfound = 1;
                daily[i] = item.Count;
            }
        });

        if (tfound == 0) {
            daily[i] = 0;
        }

        $.each(yesterdaydatas, function (k, item) {
            if (i == item.Time) {
                yfound = 1;
                yesterday[i] = item.Count;
            }
        });

        if (yfound == 0) {
            yesterday[i] = 0;
        }
    }

    var data = {
        labels: ["0h", "1h", "2h", "3h", "4h", "5h", "6h", "7h", "8h", "9h", "10h", "11h", "12h", "13h", "14h", "15h", "16h", "17h", "18h", "19h", "20h", "21h", "22h", "23h"],
        datasets: [
            {
                label: "Aujourd'hui",
                fillColor: "rgba(51, 122, 183, 1)",
                strokeColor: "rgba(51, 122, 183,1)",
                pointColor: "rgba(0,0,0,1)",
                pointHighlightStroke: "rgba(0,0,0,1)",
                data: daily
            },
            {
                label: "Hier",
                fillColor: "rgba(240, 0, 51, .2)",
                strokeColor: "rgba(240, 0, 51, .2)",
                pointColor: "rgba(0,0,0, 0.5)",
                pointHighlightStroke: "rgba(0,0,0, 0.5)",
                data: yesterday
            }
        ]
    };

    var options = {
        responsive: false,

        scaleFontColor: "#000",

        barStrokeWidth: 1.5,

        barValueSpacing: 2,

        barDatasetSpacing: 0,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,

        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(150,150,150, .3)",

        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,

        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: false,

        //String - A legend template
        //legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

    };

    var c = new Chart(ctx).Bar(data, options);
}

function _ReLoad(auth) {
    var _u = auth.split(';');

    $.ajax({
        type: 'GET',
        url: '/api/timechart',
        dataType: 'text',
        beforeSend: function (jqXHR) {
            jqXHR.setRequestHeader('Authorization', auth);
        },
        success: function (data) {
            jsonchart = JSON.parse(data);

            _DailyChart(jsonchart.PerHourToday, jsonchart.PerHourYesterday);
            _WeeklyChart(jsonchart.PerWeek);
        }
    });
}

$(function () {
    var auth = localStorage.getItem("xapiauth");
    var jsonchart = {};

    if (auth != undefined) {
        _ReLoad(auth);
    }
});