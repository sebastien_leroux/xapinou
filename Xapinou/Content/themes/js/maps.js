﻿/* Maps */

function _GeoLoc(jsonperlocation) {

    if (navigator.geolocation) {

        document.getElementById('smoking-map').innerHTML = "<div id='map'></div>";

        navigator.geolocation.getCurrentPosition(function (position) {

            // Create Map.
            var map = L.map('map').setView([position.coords.latitude, position.coords.longitude], 11);

            // OpenStreetMap layer.
            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);

            // Bind places.
            $.each(jsonperlocation, function (k, item) {
                var
                 _url = '/index/places?latitude=' + item.Latitude + '&longitude=' + item.Longitude + '&placeId=' + item.PlaceId + '&count=' + item.Count;

                $.get(_url, function (data) {
                    var marker = new L.marker([item.Latitude, item.Longitude]).addTo(map).bindPopup(data).openPopup();
                });
            });
        });
    }
}

/* Pie */

function _PlaceChart(datas) {
    var ctx = document.getElementById("places-chart").getContext("2d");

    var data = new Array();


    $.each(datas, function (k, item) {
        var p = {};

        switch (item.PlaceId) {
            case 0:
                p = {
                    "value": item.Count,
                    "color": "#FE9A2E",
                    "highlight": "#FE9A2E",
                    "label": "Définir"
                };
                break;
            case 1:
                p = {
                    "value": item.Count,
                    "color": "#5882FA",
                    "highlight": "#5882FA",
                    "label": "Maison"
                };
                break;
            case 2:
                p = {
                    "value": item.Count,
                    "color": "#FA58F4",
                    "highlight": "#FA58F4",
                    "label": "Travail"
                };
                break;
            case 3:
                p = {
                    "value": item.Count,
                    "color": "#086A87",
                    "highlight": "#086A87",
                    "label": "Ecole"
                };
                break;
            case 4:
                p = {
                    "value": item.Count,
                    "color": "#BCA9F5",
                    "highlight": "#BCA9F5",
                    "label": "Amis"
                };
                break;
            case 5:
                p = {
                    "value": item.Count,
                    "color": "#F5DA81",
                    "highlight": "#F5DA81",
                    "label": "Bar"
                };
                break;
            case 6:
                p = {
                    "value": item.Count,
                    "color": "#F78181",
                    "highlight": "#F78181",
                    "label": "Restaurant"
                };
                break;
            case 7:
                p = {
                    "value": item.Count,
                    "color": "#5F4C0B",
                    "highlight": "#5F4C0B",
                    "label": "Extérieur"
                };
                break;
            default:
                break;
        };

        data.push(p);
    });

    var options = {
        responsive: false,

        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,

        //String - The colour of each segment stroke
        segmentStrokeColor: "#fff",

        //Number - The width of each segment stroke
        segmentStrokeWidth: 2,

        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts

        //Number - Amount of animation steps
        animationSteps: 100,

        //String - Animation easing effect
        animationEasing: "easeOutBounce",

        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,

        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,

        onAnimationComplete: function () {
            this.showTooltip(this.segments, true);
        },

        //String - A legend template
        //legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };

    var c = new Chart(ctx).Pie(data, options);
}

/* Load */

function _ReLoad(auth) {
    var _u = auth.split(';');

    $.ajax({
        type: 'GET',
        url: '/api/map',
        dataType: 'text',
        beforeSend: function (jqXHR) {
            jqXHR.setRequestHeader('Authorization', auth);
        },
        success: function (data) {
            jsonmap = JSON.parse(data);

            _PlaceChart(jsonmap.PerPlace);
            _GeoLoc(jsonmap.PerLocation);
        }
    });
}

$(function () {
    var auth = localStorage.getItem("xapiauth");
    var jsonmap = {};

    if (auth != undefined) {
        _ReLoad(auth);
    }

    // GeolocMe.
    $selectme = $('.places-opts');

    $(document).on('change', '.places-opts', function (e) {
        e.preventDefault();

        $this = $(this);
        $form = $this.parents('form:first');

        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            beforeSend: function (jqXHR) {
                jqXHR.setRequestHeader('Authorization', auth);
            },
            data: $form.serialize(),
            success: function (data) {
                _ReLoad(auth);
            }
        });
    });

});