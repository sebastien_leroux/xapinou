﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using System.Net.Http;
using System.Text;
using Xapinou.Common;

namespace Xapinou.Attributes
{
    public class BasicAuthenticationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            string authorization = ParseContext(actionContext);

            if (String.IsNullOrEmpty(authorization))
            {
                actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            }
            else
            {
                if (!IsAuthorized(authorization))
                {
                    actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                }
                else
                {
                    actionContext.Request.Headers.Add("XAPIUSERID", GetUserId(authorization));
                }
            }

            base.OnActionExecuting(actionContext);
        }

        private string GetUserId(string authorization)
        {
            string[] authTab = SplitInitialAndHash(authorization);

            string initial = authTab[0];
            string userId = initial.Split(';')[0].ToString();

            return userId;
        }

        private bool IsAuthorized(string authorization)
        {
            string salt = Salt.Get();

            string[] authTab = SplitInitialAndHash(authorization);

            string initial = authTab[0];
            string hash = authTab[1];           

            string initialSalted = initial + ";" + salt;
            string initialSaltedHash = initialSalted.ToMd5();

            return (initialSaltedHash == hash);

        }

        private string ParseContext(HttpActionContext actionContext)
        {
            KeyValuePair<string, IEnumerable<string>> x = actionContext.Request.Headers.SingleOrDefault(a => a.Key == "Authorization");

            return x.Value.SingleOrDefault().ToString();
        }

        private string[] SplitInitialAndHash(string auth)
        {
            return auth.Split('|');
        }
    }
}