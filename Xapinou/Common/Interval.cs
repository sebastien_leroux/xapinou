﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xapinou.Common
{
    public class Interval
    {
        public DateTime StartDate
        {
            get;
            set;
        }

        public DateTime EndDate
        {
            get;
            set;
        }
    }
}