﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xapinou.Common
{
    public static class IntervalUtil
    {
        public static Interval Daily()
        {
            DateTime startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            DateTime endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);

            return new Interval
            {
                StartDate = startDate,
                EndDate = endDate,
            };
        }

        public static Interval Yesterday()
        {
            Interval dt = Daily();

            return new Interval
            {
                StartDate = dt.StartDate.AddDays(-1),
                EndDate = dt.EndDate.AddDays(-1),
            };
        }

        public static Interval Weekly()
        {
            int days = DateTime.Now.DayOfWeek - DayOfWeek.Sunday;

            DateTime tempStartDateWeek = DateTime.Now.AddDays(-days);
            DateTime tempEndDateWeek = tempStartDateWeek.AddDays(6);

            DateTime startDate = new DateTime(tempStartDateWeek.Year, tempStartDateWeek.Month, tempStartDateWeek.Day, 0, 0, 0);
            DateTime endDate = new DateTime(tempEndDateWeek.Year, tempEndDateWeek.Month, tempEndDateWeek.Day, 23, 59, 59);

            return new Interval
            {
                StartDate = startDate,
                EndDate = endDate,
            };
        }

        public static Interval Monthly()
        {
            DateTime tempStartDateMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            DateTime tempFirstDayOfNextMonth = tempStartDateMonth.AddMonths(1);
            DateTime tempEndDateMonth = tempFirstDayOfNextMonth.AddDays(-1);

            DateTime startDate = tempStartDateMonth;
            DateTime endDate = new DateTime(tempEndDateMonth.Year, tempEndDateMonth.Month, tempEndDateMonth.Day, 23, 59, 59);

            return new Interval
            {
                StartDate = startDate,
                EndDate = endDate,
            };
        }

        public static Interval Yearly()
        {
            DateTime startDate = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
            DateTime endDate = new DateTime(DateTime.Now.Year, 12, 31, 23, 59, 59);

            return new Interval
            {
                StartDate = startDate,
                EndDate = endDate,
            };
        }
    }
}