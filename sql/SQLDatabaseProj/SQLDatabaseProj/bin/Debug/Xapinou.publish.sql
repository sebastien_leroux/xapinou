﻿/*
Script de déploiement pour Xapinou

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "Xapinou"
:setvar DefaultFilePrefix "Xapinou"
:setvar DefaultDataPath ""
:setvar DefaultLogPath ""

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
USE [$(DatabaseName)];


GO
PRINT N'Création de [dbo].[Activity]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
CREATE TABLE [dbo].[Activity] (
    [Id]               INT        NOT NULL,
    [User_Id]          INT        NOT NULL,
    [Timestamp]        DATETIME   NOT NULL,
    [Latitude]         FLOAT (53) NOT NULL,
    [Longitude]        FLOAT (53) NOT NULL,
    [Activity_Type_Id] SMALLINT   NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Création de [dbo].[Activity_Type]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
CREATE TABLE [dbo].[Activity_Type] (
    [Id]   SMALLINT     NOT NULL,
    [Name] VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Création de [dbo].[User]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
CREATE TABLE [dbo].[User] (
    [Id]       INT          NOT NULL,
    [Name]     VARCHAR (50) NULL,
    [Password] VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Création de [dbo].[FK_Activity_ToTable]...';


GO
ALTER TABLE [dbo].[Activity] WITH NOCHECK
    ADD CONSTRAINT [FK_Activity_ToTable] FOREIGN KEY ([User_Id]) REFERENCES [dbo].[User] ([Id]);


GO
PRINT N'Création de [dbo].[FK_Activity_ToTable_1]...';


GO
ALTER TABLE [dbo].[Activity] WITH NOCHECK
    ADD CONSTRAINT [FK_Activity_ToTable_1] FOREIGN KEY ([Activity_Type_Id]) REFERENCES [dbo].[Activity_Type] ([Id]);


GO
