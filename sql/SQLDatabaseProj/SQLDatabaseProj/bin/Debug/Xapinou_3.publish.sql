﻿/*
Script de déploiement pour Xapinou

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "Xapinou"
:setvar DefaultFilePrefix "Xapinou"
:setvar DefaultDataPath ""
:setvar DefaultLogPath ""

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
USE [$(DatabaseName)];


GO
PRINT N'Suppression de [dbo].[FK_Activity_ToTable]...';


GO
ALTER TABLE [dbo].[Activity] DROP CONSTRAINT [FK_Activity_ToTable];


GO
PRINT N'Suppression de [dbo].[FK_Activity_ToTable_1]...';


GO
ALTER TABLE [dbo].[Activity] DROP CONSTRAINT [FK_Activity_ToTable_1];


GO
PRINT N'Début de la régénération de la table [dbo].[Activity]...';


GO
SET QUOTED_IDENTIFIER ON;

SET ANSI_NULLS OFF;


GO
SET QUOTED_IDENTIFIER ON;

SET ANSI_NULLS OFF;


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Activity] (
    [Id]               INT        IDENTITY (1, 1) NOT NULL,
    [User_Id]          INT        NOT NULL,
    [Timestamp]        DATETIME   NOT NULL,
    [Latitude]         FLOAT (53) NOT NULL,
    [Longitude]        FLOAT (53) NOT NULL,
    [Activity_Type_Id] SMALLINT   NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Activity])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Activity] ON;
        INSERT INTO [dbo].[tmp_ms_xx_Activity] ([Id], [User_Id], [Timestamp], [Latitude], [Longitude], [Activity_Type_Id])
        SELECT   [Id],
                 [User_Id],
                 [Timestamp],
                 [Latitude],
                 [Longitude],
                 [Activity_Type_Id]
        FROM     [dbo].[Activity]
        ORDER BY [Id] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Activity] OFF;
    END

DROP TABLE [dbo].[Activity];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Activity]', N'Activity';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Début de la régénération de la table [dbo].[Activity_Type]...';


GO
SET QUOTED_IDENTIFIER ON;

SET ANSI_NULLS OFF;


GO
SET QUOTED_IDENTIFIER ON;

SET ANSI_NULLS OFF;


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Activity_Type] (
    [Id]   SMALLINT     IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Activity_Type])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Activity_Type] ON;
        INSERT INTO [dbo].[tmp_ms_xx_Activity_Type] ([Id], [Name])
        SELECT   [Id],
                 [Name]
        FROM     [dbo].[Activity_Type]
        ORDER BY [Id] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Activity_Type] OFF;
    END

DROP TABLE [dbo].[Activity_Type];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Activity_Type]', N'Activity_Type';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Début de la régénération de la table [dbo].[User]...';


GO
SET QUOTED_IDENTIFIER ON;

SET ANSI_NULLS OFF;


GO
SET QUOTED_IDENTIFIER ON;

SET ANSI_NULLS OFF;


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_User] (
    [Id]       INT          IDENTITY (1, 1) NOT NULL,
    [Name]     VARCHAR (50) NULL,
    [Password] VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[User])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_User] ON;
        INSERT INTO [dbo].[tmp_ms_xx_User] ([Id], [Name], [Password])
        SELECT   [Id],
                 [Name],
                 [Password]
        FROM     [dbo].[User]
        ORDER BY [Id] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_User] OFF;
    END

DROP TABLE [dbo].[User];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_User]', N'User';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
PRINT N'Création de [dbo].[FK_Activity_ToTable]...';


GO
ALTER TABLE [dbo].[Activity] WITH NOCHECK
    ADD CONSTRAINT [FK_Activity_ToTable] FOREIGN KEY ([User_Id]) REFERENCES [dbo].[User] ([Id]);


GO
PRINT N'Création de [dbo].[FK_Activity_ToTable_1]...';


GO
ALTER TABLE [dbo].[Activity] WITH NOCHECK
    ADD CONSTRAINT [FK_Activity_ToTable_1] FOREIGN KEY ([Activity_Type_Id]) REFERENCES [dbo].[Activity_Type] ([Id]);


GO
