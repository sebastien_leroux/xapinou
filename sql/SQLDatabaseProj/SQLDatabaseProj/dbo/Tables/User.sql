﻿CREATE TABLE [dbo].[User] (
    [Id]       INT          NOT NULL	IDENTITY (1,1),
    [Name]     VARCHAR (50) NULL,
    [Password] VARCHAR (50) NULL,
    [Token] VARCHAR(255) NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

