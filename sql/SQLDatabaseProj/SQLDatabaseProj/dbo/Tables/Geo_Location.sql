﻿CREATE TABLE [dbo].[Geo_Location]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY (1,1), 
    [Latitude] FLOAT NOT NULL, 
    [Longitude] FLOAT NOT NULL, 
    [User_Id] INT NOT NULL, 
    [Place_Id] SMALLINT NOT NULL
)
