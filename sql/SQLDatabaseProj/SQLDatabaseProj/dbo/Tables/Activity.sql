﻿CREATE TABLE [dbo].[Activity] (
    [Id]               INT        NOT NULL	IDENTITY(1,1),
    [User_Id]          INT        NOT NULL,
    [Timestamp]        DATETIME   NOT NULL,
    [Activity_Type_Id] SMALLINT   NOT NULL,
    [Geo_Location_Id] INT NOT NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Activity_ToTable] FOREIGN KEY ([User_Id]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_Activity_ToTable_1] FOREIGN KEY ([Activity_Type_Id]) REFERENCES [dbo].[Activity_Type] ([Id]), 
    CONSTRAINT [FK_Activity_ToTable_2] FOREIGN KEY ([Geo_Location_Id]) REFERENCES [dbo].[Geo_Location] ([Id])
);

