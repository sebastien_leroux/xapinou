﻿CREATE TABLE [dbo].[Activity_Type] (
    [Id]   SMALLINT     NOT NULL	IDENTITY(1,1),
    [Name] VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

